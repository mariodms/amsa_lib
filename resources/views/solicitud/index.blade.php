@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ url('../css/dropzone.css') }}">
<link rel="stylesheet" href="{{ url('../css/custom.css') }}">
@endsection
@section('title')
Carga documental
@endsection
@section('content')
    <!-- Start content -->
    <div class="content">
        <div class="container">
          <br>
          <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="card-box widget-box-two widget-two-custom">
                  <i class="dripicons-clock widget-two-icon"></i>
                    <div class="wigdet-two-content">
                      <p class="m-0 text-uppercase text-white font-600 text-overflow" title="Statistics">Pendientes</p>
                        <h2 class="text-white"><span data-plugin="counterup">34578</span> </h2>
                    </div>
                </div>
          </div><!-- end col -->
          <div class="col-lg-3 col-md-6">
              <div class="card-box widget-box-two widget-two-warning">
                  <i class="dripicons-archive widget-two-icon"></i>
                  <div class="wigdet-two-content">
                      <p class="m-0 text-white text-uppercase font-600 text-overflow" title="User Today">En Revisión</p>
                      <h2 class="text-white"><span data-plugin="counterup">6352</span> </h2>
                  </div>
              </div>
          </div><!-- end col -->

         <div class="col-lg-3 col-md-6">
            <div class="card-box widget-box-two widget-two-inverse">
                <i class="dripicons-checkmark widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase text-white font-600 text-overflow" title="User This Month">Revisados</p>
                    <h2 class="text-white"><span data-plugin="counterup">58742</span> </h2>
                </div>
            </div>
        </div><!-- end col -->

        <div class="col-lg-3 col-md-6">
            <div class="card-box widget-box-two widget-two-danger">
                <i class="dripicons-warning widget-two-icon"></i>
                <div class="wigdet-two-content">
                    <p class="m-0 text-white text-uppercase font-600 text-overflow" title="Request Per Minute">Rechazados</p>
                    <h2 class="text-white"><span data-plugin="counterup">12365</span> </h2>
                </div>
            </div>
        </div><!-- end col -->

          </div>
            <div class="row">
              <div class="row" id="datos_reporte_empresas_externas">
                  <div class="col-sm-12">
                      <div class="card-box table-responsive">
                          <h4 class="m-t-0 header-title"><b>Solicitudes</b></h4>
                          <p class="text-muted font-14 m-b-30">
                              Se presenta listado de solicitudes realizadas.
                          </p>

                          <table id="table_datos_emp_ext" class="table table-striped table-bordered">
                              <thead>
                              <tr>
                                  <th>Nº Solicitud</th>
                                  <th>Empresa</th>
                                  <th>Estado</th>
                                  <th>Fecha creación</th>
                                  <th>Fecha Actualización</th>
                                  <th>Acción</th>
                                  <th>Motivo Rechazo</th>
                              </tr>
                              </thead>
                              <tbody>
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
            </div>

            <br>
            <!-- <div class="row">
                <div class="col-md-4">
                    <button id="boton_reporte" type="button" class="btn btn-custom waves-effect waves-light"> <i class="fi-file-subtract"></i> <span>Generar Reporte</span> </button>
                    <button id="boton_reporte_cargando" type="button" class="btn btn-custom waves-effect waves-light hidden" disabled="true"> <i class="fa fa-spin fa-spinner"></i> <span>Generando...</span> </button>
                </div>
            </div> -->








        </div> <!-- container -->
    </div> <!-- content -->

@endsection

@section('scripts')
<script src="{{ url('../js/jquery.js') }}"></script>
<script src="{{ url('../js/dropzone.js') }}"></script>
<script src="{{ url('../js/dropzone-config.js') }}"></script>
@endsection
