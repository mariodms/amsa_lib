@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ url('../css/dropzone.css') }}">
<link rel="stylesheet" href="{{ url('../css/custom.css') }}">
@endsection

@section('scripts')
<script type="text/javascript">
$('#enviar').click(function(){
  var id_sol = '{{ Crypt::encryptString($solicitud->id) }}';

  $.ajax({
    type: 'POST',
    url: '/document_exist/validador',
    data: {id_sol: id_sol, _token: '{{csrf_token()}}'},
    success: function(data){
        if(data == 0){
          $("#error").modal('show');
           $("html, body").animate({ scrollTop: 0 }, "slow");
          location.reload();
        }else{
          $("#pass").modal('show');
        }
    }
  });
});
</script>
<script src="{{ url('../js/dropzone.js') }}"></script>
<script src="{{ url('../js/dropzone-config.js') }}"></script>
@endsection

@section('title')
Validar solicitud
@endsection
@section('content')
<div class="content">
   <div class="container">
      <br>
      <div class="col-lg-12">

      <div class="row">

        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/organization.svg" alt="organization.svg">
              </div>
              <div class="text-right">
                  <p class="m-t-5 text-uppercase font-14 font-600">{{$empresa->razonsocial_eecc}}</p>
                  <p class="m-t-5 text-uppercase font-14 font-600">{{$contrato->emailadc_eecc}}</p>
              </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/diploma_2.svg" alt="diploma_2.svg">
              </div>
              <div class="text-right">
                  <p class="m-t-5 text-uppercase font-14 font-600">Contrato N° {{$contrato->numero_ctto}}</p>
                  <p class="m-t-5 text-uppercase font-14 font-600">{{ $compania->descripcion }}</p>
              </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/businessman.svg" alt="businessman.svg">
              </div>
              <div class="text-right">
                  <p class="m-t-5 text-uppercase font-14 font-600">{{$contrato->adc_eecc}}</p>
                  <p class="m-t-5 text-uppercase font-14 font-600">{{$contrato->emailadc_eecc}}</p>
              </div>
          </div>
        </div>


      </div>
    </div>
      <div class="col-lg-12">
         <div class="panel panel-color panel-inverse">
            <!-- Default panel contents -->
            <div class="panel-heading">
               <h3 class="panel-title"><img class="icon-colored" src="/images/icons/folder.svg" title="organization.svg"> Información documental cargada</h3>
            </div>
            <!-- Table -->
            <table class="table table-bordered">
               <thead>
                  <tr>
                     <th>Documento</th>
                     <th>Acción</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach ($documentos as $key => $value)
                  <?php $tipo = App\Tipo_doc::find($value->fkid_tipo); ?>
                  <tr>
                     <td> {{ $value->fkid_tipo != 6 ? 'Documento N° '.$tipo->id : $tipo->descripcion }}</td>
                     <td class="">
                        <a href="{{ url('descarga/'. Crypt::encryptString($value->id)) }}" class="btn btn-primary waves-effect waves-light">Descargar <i class="fa fa-download m-r-5"></i></a>
                     </td>
                  </tr>
                  @endforeach
               </tbody>
            </table>
         </div>
      </div>
      @if( Auth::user()->fkid_perfil == "2")
      <div class="col-lg-6">
         <div class="panel panel-border panel-inverse">
            <div class="panel-heading">
               <h3 class="panel-title">Subir Certificado</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                  <div class="col-sm-12 offset-sm-1">
                    <h2 class="page-heading">Cargar Certificado<span id="counter"></span></h2>
                    <form method="post" action="{{ url('/cargraCertificado/'.Crypt::encryptString($solicitud->id)) }}"
                    enctype="multipart/form-data" class="dropzone" id="my-dropzone">
                      {{ csrf_field() }}
                      <div class="dz-message">
                        <div class="col-xs-12">
                          <div class="message">
                            <p>Arrastra el documento o haz click para selecionar</p>
                          </div>
                        </div>
                      </div>
                      <div class="fallback hidden">
                        <input type="file" name="file" multiple>
                      </div>
                    </form>
                  </div>
                  {{--Dropzone Preview Template--}}
                  <div id="preview" style="display: none;">
                    <div class="dz-preview dz-file-preview">
                      <div class="dz-image"><img data-dz-thumbnail /></div>
                      <div class="dz-details">
                        <div class="dz-size"><span data-dz-size></span></div>
                        <div class="dz-filename"><span data-dz-name></span></div>
                      </div>
                      <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                      <div class="dz-error-message"><span data-dz-errormessage></span></div>
                      <div class="dz-success-mark">
                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                          <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                          <title>Check</title>
                          <desc>Created with Sketch.</desc>
                          <defs></defs>
                          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                            <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                          </g>
                        </svg>
                      </div>
                      <div class="dz-error-mark">
                        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                        <title>error</title>
                        <desc>Created with Sketch.</desc>
                        <defs></defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                        <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                        </g>
                        </g>
                        </svg>
                      </div>
                    </div>
                  </div>
              </div>
              <br>
              <div class="row">
               <button id='enviar' type="button" class="btn btn-success waves-effect waves-light">
                 <i class="fa fa-check-circle m-r-5"></i>
                 <span>Subir Certificado</span>
               </button>
               <!-- <a href="#custom-modal" class="btn btn-success waves-effect w-md m-r-5 m-b-10" data-animation="blur" data-plugin="custommodal"
                 data-overlaySpeed="100" data-overlayColor="#36404a">  <i class="fa fa-up m-r-5"></i> Subir Certificado</a>
               </div> -->
             </div>
            </div>
         </div>
      </div>
      @endif
   </div>
   <!-- container -->
</div>
<div id="pass" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Enviar solicitud a revisión</h4>
            </div>
            <div class="modal-body">
                <h4>Está a punto de enviar la solicitud a revisión.</h4>
                <p>Si posee toda la documentación obligatoria cargada</p>
                <p>declaración de veracidad</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
              <button  type="button" class="btn btn-primary waves-effect waves-light" onclick="location.href='{{ url('/finalizar-solicitud/'.Crypt::encryptString($solicitud->id)) }}'">Enviar solicitud</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id='error' class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
<div class="modal-dialog modal-sm">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="mySmallModalLabel">Documentación obligatoria</h4>
      </div>
      <div class="modal-body">
        <p>No ha cargado toda la documentación obligatoria</p>
      </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection()
