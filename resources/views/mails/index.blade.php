@extends('layouts.app')

@section('content')
<div class="content">
 <div class="container" >

   @if (Session::has('success'))

   <div class="alert alert-success" role="alert">
   <strong>Bien :</strong> {{ Session::get('success') }}
   </div>

   @endif
    <h2>Enviar Correos</h2>


        {{ csrf_field() }}

        @foreach($data['usuarios'] as $usuario)
        @if($usuario->fkid_empresa != NULL && $usuario->fkid_contrato != NULL)
        <?php
          $ctto = App\Contrato::where('id','=',$usuario->fkid_contrato)->first();
          $emp = App\Empresa::where('id','=',$usuario->fkid_empresa)->first();
        ?>
        @if($ctto->fkid_compania == 1 && $usuario->user_apellido !== '1' )
        <div class="col-sm-12">
          <div class="card-box table-responsive">
        <form method="post" action="{!! url('/mails/usuarios') !!}" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>


        <div class="form-group row">
            <label for="email" class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-9">
                <input name="email" type="text" class="form-control" id="email" value="{{$ctto->emailadc_eecc}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-sm-3 col-form-label">Usuario</label>
            <div class="col-sm-9">
                <input name="name" type="text" class="form-control" id="name" value="{{$usuario->name}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="password" class="col-sm-3 col-form-label">Contraseña</label>
            <div class="col-sm-9">
                <input name="password" type="text" class="form-control" id="password" value="123456">
            </div>
        </div>

        <div class="form-group row">
            <div class="offset-sm-3 col-sm-9">
                <button id="boton_enviar" type="submit" class="btn btn-custom waves-effect waves-light"><i class="fi-mail"></i> <span>Enviar Correo</span></button>
                <button id="boton_enviar_cargando" class="btn btn-custom waves-effect waves-light hidden" disabled="true"><i class="fa fa-spinner fa-spin"></i> <span>Enviando Correo...</span></button>

            </div>
        </div>
        <div class="form-group row hidden">
            <label for="id_user" class="col-sm-3 col-form-label">Contraseña</label>
            <div class="col-sm-9">
                <input name="id_user" type="text" class="form-control" id="id_user" value="{{$usuario->id}}">
            </div>
        </div>


    </form>
    </div>
  </div>
      @endif
      @endif
      @endforeach
</div></div>
@endsection
@section('scripts')
<script src="../helpers/helper.js"></script>

    <script src="../plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>
    <script src="../plugins/jquery-datatables-editable/jquery.dataTables.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="../plugins/tiny-editable/mindmup-editabletable.js"></script>
    <script src="../plugins/tiny-editable/numeric-input-example.js"></script>

    <script src="../plugins/datatables/dataTables.buttons.js"></script>

    <script src="../plugins/datatables/jszip.min.js"></script>
    <script src="../plugins/datatables/pdfmake.min.js"></script>
    <script src="../plugins/datatables/vfs_fonts.js"></script>
    <script src="../plugins/datatables/buttons.html5.js"></script>

    <script src="{!! asset('pages/jquery.datatables.editable.init.js') !!}" type="text/javascript"></script>

    <script type="text/javascript">


$("#boton_enviar").click(function(){

    $(this).addClass("hidden");
    $("#boton_enviar_cargando").removeClass("hidden");
  });
</script>
@endsection
