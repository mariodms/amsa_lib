@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="../plugins/jquery-datatables-editable/datatables.css" />
    <link rel="stylesheet" href="../plugins/magnific-popup/css/magnific-popup.css" />
    <link rel="stylesheet" href="../plugins/datatables/buttons.dataTables.css" />
@endsection
@section('content')
<!-- Start content -->
<div class="content">
 <div class="container" >

      <div class="col-sm-12">
          <div class="card-box table-responsive">
              <h4 class="m-t-0 header-title"><b>Usuarios</b></h4>
              <p class="text-muted font-14 m-b-30">
                  Se Crearán usuarios.
              </p>
              <table id="datatable-buttons" class="table table-striped table-bordered">
                  <thead>
                  <tr>
                    <th>name</th>
                    <th>email</th>
                    <th>password</th>
                    <th>user_nombre</th>
                    <th>user_apellido</th>
                    <th>fkid_empresa</th>
                    <th>fkid_contrato</th>
                    <th>fkid_perfil</th>




                  </tr>
                  </thead>
                  <tbody>

                    @foreach($data["id_contratos"] as $contrato)
                    <?php
                      $emp = App\Empresa::where('id','=',$contrato->fkid_empresa)->first();
                      $compania = App\Compania::where('id','=',$contrato->fkid_compania)->first();
                      $usuario = App\User::where('fkid_contrato','=',$contrato->id)->first();
                      // dd($usuario);


                      $razonsocial_emp =str_limit($emp->razonsocial_eecc,$limit = 3,$end = '');
                      $n_contrato = substr($contrato->numero_ctto,$limit = -3,3);
                      $des_compania = str_limit($compania->descripcion,$limit = 3,$end = '');
                      $new_user= strtoupper($razonsocial_emp."".$n_contrato."".$des_compania."".$contrato->id);

                    ?>
                    <tr>
                    @if($usuario == null)

                    <th>{{$new_user}}</th>
                    <th>{{$new_user}}</th>
                    <th>$2y$10$p0riYQ7IhUjQ.4xRAODr.eZho/EtjcZ08Y26AuAtx2CKodcvEziOS</th>
                    <th>{{$contrato->adc_eecc}}</th>
                    <th>NULL</th>
                    <th>{{$contrato->fkid_empresa}}</th>
                    <th>{{$contrato->id}}</th>
                    <th>1</th>
                    <?php
                    DB::table('users')->insert(
                        ['name' => $new_user, 'email' => $new_user ,'password'=>'$2y$10$p0riYQ7IhUjQ.4xRAODr.eZho/EtjcZ08Y26AuAtx2CKodcvEziOS', 'user_nombre'=>$contrato->adc_eecc,'fkid_empresa' =>$contrato->fkid_empresa, 'fkid_contrato'=>$contrato->id]

                    );
                    ?>
                    @else
                    <th>No</th>
                    <th>No</th>
                    <th>No</th>
                    <th>No</th>
                    <th>No</th>
                    <th>No</th>
                    <th>No</th>
                    <th>No</th>
                    @endif

                      </tr>
                    @endforeach


                  </tbody>
              </table>
          </div>
      </div>



        <br>
</div>
</div>
<!-- content -->
@endsection

@section('scripts')

    <script src="../helpers/helper.js"></script>

    <script src="../plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>
    <script src="../plugins/jquery-datatables-editable/jquery.dataTables.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="../plugins/tiny-editable/mindmup-editabletable.js"></script>
    <script src="../plugins/tiny-editable/numeric-input-example.js"></script>

    <script src="../plugins/datatables/dataTables.buttons.js"></script>

    <script src="../plugins/datatables/jszip.min.js"></script>
    <script src="../plugins/datatables/pdfmake.min.js"></script>
    <script src="../plugins/datatables/vfs_fonts.js"></script>
    <script src="../plugins/datatables/buttons.html5.js"></script>

    <script src="{!! asset('pages/jquery.datatables.editable.init.js') !!}" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function () {
          var estado_solicitud =1;
          $("#boton_reporte").trigger("click");

            //generar reporte al cargar
            $("#boton_reporte").ready(function(){

                $.ajax({
                    url: '/tabla-solicitudes',
                    type: 'GET',
                    data: {
                        estado_solicitud: estado_solicitud

                    }

                }).done(function(response){

                    $("#datos_reporte").html(response);

                    $("#datatable-buttons").DataTable({
                      "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                      },
                        dom: "Bfrtip",
                        buttons: [{
                            extend: "excel",
                            text: 'Exportar a Excel',
                            className: "btn-sm"
                        }, {
                            extend: "pdf",
                            text: 'Exportar a PDF',
                            className: "btn-sm"
                        }, {
                            extend: "print",
                            text: 'Imprimir Reporte',
                            className: "btn-sm"
                        }],
                        "order": [[ 7, "desc" ]]
                    });
                });
            });




        });
    </script>

@endsection
