<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metisMenu nav" id="side-menu">
                <li class="menu-title ">Menú</li>


                @if(Auth::user()->fkid_perfil == "1")
                    <li class="">
                        <a href="{!! url('/') !!}"><i class="fi-air-play"></i><span> Dashboard </span> </a>
                    </li>
                <!–– Perfil empresa ->
                    <li>
                        <a href="javascript: void(0);" aria-expanded="true"><i class="fi-bar-graph-2"></i><span>Gestión documental</span> <span class="menu-arrow"></span></a>
                        <ul class="nav-second-level nav" aria-expanded="true">
                            <li class="">
                                <a href="{!! url('/inicio') !!}">Inicio</a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if( Auth::user()->fkid_perfil == "2")
                <!–– Perfil validador ->
                    <li>
                        <a href="javascript: void(0);" aria-expanded="true"><i class="fi-bar-graph-2"></i><span> Validar solicitudes</span><span class="menu-arrow"></span></a>
                        <ul class="nav-second-level nav" aria-expanded="true">
                            <li class="">
                                <a href="{!! url('/')!!}">Todas las solicitudes</a>
                            </li>
                            <li class="">
                                <a href="{!! url('/solicitud-no-enviada')!!}">Listado de solicitudes no enviadas</a>
                            </li>
                            <li class="">
                                <a href="{!! url('/solicitud-pendiente')!!}">Listado de solicitudes pendientes</a>
                            </li>
                            <li class="">
                              <a href="{!! url('/solicitud-finalizada')!!}">Listado de solicitudes finalizada</a>
                            </li>
                        </ul>
                    </li>
                @endif
                @if( Auth::user()->fkid_perfil == "3")
                <!–– Perfil mandante ->
                  <li>
                      <a href="javascript: void(0);" aria-expanded="true"><i class="fi-bar-graph-2"></i><span> Solicitudes </span><span class="menu-arrow"></span></a>
                      <ul class="nav-second-level nav" aria-expanded="true">
                          <li class="">
                              <a href="{!! url('/solicitud-no-enviada')!!}">Listado de solicitudes no enviadas</a>
                          </li>
                          <li class="">
                              <a href="{!! url('/solicitud-pendiente')!!}">Listado de solicitudes pendientes</a>
                          </li>
                          <li class="">
                            <a href="{!! url('/solicitud-finalizada')!!}">Listado de solicitudes finalizada</a>
                          </li>
                          <li class="">
                            <a href="{!! url('/solicitudes')!!}">Listado de solicitudes</a>
                          </li>
                      </ul>
                  </li>
                @endif

            </ul>

        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
