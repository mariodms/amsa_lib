<div class="topbar">
    <div class="topbar-left">
      <a href="{!! url('/') !!}" class="logo">
                        <span>
                            <img src="{{asset('../images/logo_amsa.png')}}" alt="" height="40">
                        </span>
            <i>
                <img src="{{asset('../images/logo_amsa_sm.png')}}" alt="" height="18">
            </i>
        </a>
    </div>
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <ul class="nav navbar-nav navbar-left">
                <li>
                    <button type="button" class="button-menu-mobile open-left waves-effect">
                        <i class="dripicons-menu"></i>
                    </button>
                </li>
                <li>
                    <a href="#" class="right-menu-item dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user-circle-o"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-lg dropdown-menu-left user-list notify-list">
                        <li class="list-group notification-list m-b-0">
                            <div class="slimscroll">
                                <!-- list item-->
                                <a href="javascript:void(0);" class="list-group-item">
                                    <div class="media">
                                        <div class="media-left p-r-10">
                                            <em class="fi-mail bg-primary"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 id="change_email" class="media-heading">{!! Auth::user() ->email !!}</h5>
                                            <p class="m-0">
                                                Email soporte: marioossandon@workmate.cl
                                            </p>
                                        </div>
                                    </div>
                                </a>

                              <a href="javascript:void(0);" class="list-group-item" >
                                    <div class="media">
                                        <div class="media-left p-r-10">
                                            <em class="mdi mdi-clipboard-text bg-custom"></em>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="media-heading">Proyectos</h5>
                                            <p class="m-0">
                                                <span class="text-primary font-600">AMSA</span>
                                            </p>
                                        </div>
                                    </div>
                                </a>
                                @if(Auth::user()->fkid_perfil == 1)
                                <a href="{{  url('contrato/'. Crypt::encryptString($contrato->id)) }}" class="list-group-item" >
                                      <div class="media">
                                          <div class="media-left p-r-10">
                                              <em class="mdi mdi-clipboard-text bg-custom"></em>
                                          </div>
                                          <div class="media-body">
                                              <h5 class="media-heading">Actualizar info contrato</h5>
                                              <p class="m-0">
                                                  <span class="text-primary font-600">{{$contrato->numero_ctto}}</span>
                                              </p>
                                          </div>
                                      </div>
                                  </a>
                                  @endif
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="hidden-xs vertical-middle">
                    <br/>
                    <h4 class="page-title"> {!! Auth::user()->user_nombre !!}</h4>
                </li>
            </ul>
          <ul class="nav navbar-nav navbar-right">
                <li class="dropdown user-box">
                    <a href="" class="dropdown-toggle waves-effect user-link" data-toggle="dropdown"
                       aria-expanded="true">
                        <img src="{{asset('../images/generic_avatar.jpg')}}" alt="user-img" class="img-circle user-img">
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                        <li class=""></li>
                        <li><a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Cerrar sesión') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
