<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Control de acceso | @yield('title') </title>

    <!-- DataTables -->
<!--    <link href="../plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>-->
    <link href="../plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="../plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="../plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <link href="../plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="{!! asset('plugins/custombox/css/custombox.min.css') !!}" rel="stylesheet">
    <link href="{!! asset('plugins/switchery/switchery.min.css') !!}" rel="stylesheet" >
    <link href="../plugins/sweet-alert2/sweetalert2.min.css" rel="stylesheet" type="text/css">

    <link href="../plugins/jquery-toastr/jquery.toast.min.css" rel="stylesheet" type="text/css">


    <!-- Plugins css -->
    <link href="../plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="../plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="../plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="../plugins/clockpicker/css/bootstrap-clockpicker.min.css" rel="stylesheet">
    <link href="../plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">


    <!-- App css -->
    <link href="../plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="{!! asset('css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/core.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/components.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/icons.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/pages.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/menu.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/responsive.css') !!}" rel="stylesheet" type="text/css" />
    <link href="../../../plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
    <script src="{!! asset('js/modernizr.min.js') !!}"></script>

    @section('css')
    @show
</head>
<body>

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- TOP BAR -->
        @include('layouts.topnavbar')

        <!-- NAVIGATION -->
        @include('layouts.navigation')

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="content-page">
            <!-- Main view  -->
                @yield('content')
                @include('includes.footer')
        </div>

    </div>
    <!-- End wrapper-->
  <script src="{!! asset('js/jquery.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('js/bootstrap.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('js/metisMenu.min.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('js/waves.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('js/jquery.slimscroll.js') !!}" type="text/javascript"></script>

  <script src="../plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>

  <script src="../../../plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
  <script src="{!! asset('helpers/helper.js') !!}" type="text/javascript"></script>
  <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
  <script src="../plugins/datatables/buttons.bootstrap.min.js"></script>
  <script src="../plugins/datatables/jszip.min.js"></script>
  <script src="../plugins/datatables/pdfmake.min.js"></script>
  <script src="../plugins/datatables/vfs_fonts.js"></script>
  <script src="../plugins/datatables/buttons.html5.min.js"></script>
  <script src="../plugins/datatables/buttons.print.min.js"></script>
  <script src="../plugins/datatables/dataTables.fixedHeader.min.js"></script>
  <script src="../plugins/datatables/dataTables.keyTable.min.js"></script>
  <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
  <script src="../plugins/datatables/responsive.bootstrap.min.js"></script>
  <script src="../plugins/datatables/dataTables.scroller.min.js"></script>
  <script src="../plugins/datatables/dataTables.colVis.js"></script>
  <script src="../plugins/datatables/dataTables.fixedColumns.min.js"></script>

  <script src="../plugins/moment/moment.js"></script>
  <script src="../plugins/timepicker/bootstrap-timepicker.js"></script>
  <script src="../plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
  <script src="../plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
  <script src="../plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
  <script src="../plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="../plugins/select2/js/select2.min.js" type="text/javascript"></script>

  <!-- init  -->
  <script src="{!! asset('pages/jquery.datatables.init.js') !!}" type="text/javascript"></script>

  <!-- Sweet-Alert  -->
  <script src="{!! asset('plugins/sweet-alert2/sweetalert2.min.js') !!}" type="text/javascript"></script>

  <!-- trast-->
  <script src="{!! asset('plugins/jquery-toastr/jquery.toast.min.js') !!}" type="text/javascript"></script>
  <!--<script src="{!! asset('pages/jquery.sweet-alert.init.js') !!}" type="text/javascript"></script>-->

  <!-- Modal-Effect -->
  <script src="{!! asset('plugins/custombox/js/custombox.min.js') !!}"></script>
  <script src="{!! asset('plugins/switchery/switchery.min.js') !!}"></script>


  <script src="{!! asset('plugins/custombox/js/custombox.min.js') !!}"></script>
  <!-- Counter js  -->
  <script src="../plugins/waypoints/jquery.waypoints.min.js"></script>
  <script src="../plugins/counterup/jquery.counterup.min.js"></script>


  <!-- App js -->
  <script src="{!! asset('js/jquery.core.js') !!}" type="text/javascript"></script>
  <script src="{!! asset('js/jquery.app.js') !!}" type="text/javascript"></script>

  <script type="text/javascript">

      var resizefunc = [];

      $(document).ready(function () {
          $('#alert').click(function(event) {
              event.preventDefault();
              var correo = $('#correo').val();

              swal({
                  title: 'Editar Correo Electronico',
                  input: 'email',
                  showCancelButton: true,
                  confirmButtonText: 'Actualizar',
                  showLoaderOnConfirm: true,
                  preConfirm: function (email) {
                      return new Promise(function (resolve, reject) {
                          setTimeout(function() {
                              if (email === correo ) {
                                  reject('El correo ingresado debe ser distinto al anterior.')
                              } else {
                                  resolve()
                                  $.ajax({
                                      url: "/actualizar-email",
                                      type : 'PUT',
                                      data: {
                                          'email' :email ,
                                          "_token": "{!! csrf_token() !!}"
                                      },
                                      success:function (data) {
                                          if(data == "ok"){
                                              $('#change_email').html(email);
                                              $('#correo').html(email);
                                          }else {
                                              sweetAlert("Oops...", "Something went wrong!", "error");
                                          }
                                      }
                                  });
                              }
                          }, 2000)
                      })
                  },

                  allowOutsideClick: false
              }).then(function (email) {
                  swal({
                      type: 'success',
                      title: 'Su correo ha sido actualizo!',
                      html: 'El nuevo correo es : ' + email

                  })
                  $('#correo').html(email);
              })

          });
      });
  </script>

  @section('scripts')
  @show
</body>
</html>
