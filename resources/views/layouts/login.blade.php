<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="icon" type="image/png" href="{{asset('../images/logo_amsa_sm.png')}}" />
  <title>Biblioteca Documental | AMSA</title>
@include('includes.head')
</head>
@yield('javascript')
<body class="page-body login-page login-form-fall loaded login-form-fall-init"  data-url="http://neon.dev">
@yield('content')
</body>
{{--@include('includes.footer')--}}
</html>
