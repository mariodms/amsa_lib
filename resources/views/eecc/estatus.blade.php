@extends('layouts.app')

@section('css')

@endsection

@section('scripts')

@endsection

@section('title')
Solicitud enviada a revisión
@endsection

@section('content')
    <!-- Start content -->
<div class="content">
    <div class="container">
      <br>
      <div class="row">

        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/organization.svg" alt="organization.svg">
              </div>
              <div class="text-right">
                  <p class="m-t-5 text-uppercase font-14 font-600">{{$empresa->razonsocial_eecc}}</p>
                  <p class="m-t-5 text-uppercase font-14 font-600">{{$contrato->emailadc_eecc}}</p>
              </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/diploma_2.svg" alt="diploma_2.svg">
              </div>
              <div class="text-right">
                  <p class="m-t-5 text-uppercase font-14 font-600">Contrato N° {{$contrato->numero_ctto}}</p>
                  <p class="m-t-5 text-uppercase font-14 font-600">{{ $compania->descripcion }}</p>
              </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/businessman.svg" alt="businessman.svg">
              </div>
              <div class="text-right">
                  <p class="m-t-5 text-uppercase font-14 font-600">Correo Soporte</p>
                  <p class="m-t-5 text-uppercase font-14 font-600">marioossandon@workmate.cl</p>
              </div>
          </div>
        </div>


      </div>

      @if($not)
      <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <i class="mdi mdi-check-all"></i>
        <strong>Solicitud enviada!</strong> Se ha ingresado correctamente la solicitud en plataforma y ahora se encuentra pendiente de revisión.
      </div>
      @endif
      <div class="row">
        @if($documentos != null)
        <div class="col-lg-6">
           <div class="panel panel-color panel-inverse">
              <!-- Default panel contents -->
              <div class="panel-heading">
                 <h3 class="panel-title"><img class="icon-colored" src="/images/icons/folder.svg" title="organization.svg"> Información documental cargada</h3>
              </div>
              <!-- Table -->
              <table class="table table-bordered">
                 <thead>
                    <tr>
                       <th>Documento</th>
                       <th>Acción</th>
                    </tr>
                 </thead>
                 <tbody>
                    @foreach ($documentos as $key => $value)
                    <?php $tipo = App\Tipo_doc::find($value->fkid_tipo); ?>
                    <tr>
                       <td> {{ $value->fkid_tipo != 6 ? 'Documento N° '.$tipo->id : $tipo->descripcion }}</td>
                       <td class="">
                          <a href="{{ url('descarga/'. Crypt::encryptString($value->id)) }}" class="btn btn-primary waves-effect waves-light">Descargar <i class="fa fa-download m-r-5"></i></a>
                       </td>
                    </tr>
                    @endforeach
                 </tbody>
              </table>
           </div>
        </div>
        <div class="col-lg-6">
								<div class="panel panel-color panel-inverse">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<h3 class="panel-title"><img class="icon-colored" src="/images/icons/folder.svg" title="organization.svg"> Solicitud</h3>
									</div>
									<div class="panel-body">
										<p>
                      La solicitud ya se encuentra correctamente ingresa con la documentación obligatoria.
										</p>
									</div>

									<!-- Table -->
									<table class="table">
										<thead>
											<tr>
												<th>N° de solicitud</th>
												<th>N° de contrato</th>
												<th>Empresa</th>
												<th>Compañia</th>
                        <th>Acción</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<th scope="row">{{ $solicitud->id }}</th>
												<td>{{ $contrato->numero_ctto }}</td>
												<td>{{ $empresa->razonsocial_eecc}}</td>
												<td>{{ $compania->descripcion }}</td>
                        <td><a href="{{ ($solicitud->estado_solicitud == 3 || $solicitud->estado_solicitud == 2 || $solicitud->edit==0 )? '' : url('/editar/'. Crypt::encryptString($solicitud->id)) }}" class="btn {{ ($solicitud->estado_solicitud == 1 || $solicitud->estado_solicitud == 0  ) && $solicitud->edit == 1 ? 'btn btn-info waves-effect w-md waves-light' : 'btn btn-warning waves-effect w-md waves-light'  }}"  >{{($solicitud->estado_solicitud == 1 || $solicitud->estado_solicitud == 0  ) && $solicitud->edit == 1 ?  'Editar solicitud' : 'Edición no disponible'}} </a></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
        @endif
      </div>
      <div class="row">

      </div>
      <br>
@endsection
