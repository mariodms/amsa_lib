@extends('layouts.app')

@section('css')

@endsection

@section('scripts')
<script type="text/javascript">

$( "#password-confirm" ).keyup(function( event ) {
  if ($('#password-confirm').val() == $('#password').val() && $('#password-confirm').val().length > 0 && $('#password').val().length > 0) {
    $('#submit-pass').prop('disabled', false);
    $('#p-hidden').prop('hidden', true);
  }else {
    $('#submit-pass').prop('disabled', true);
    $('#p-hidden').prop('hidden', false);
  }
}).keydown(function( event ) {
  if ( event.which == 13 ) {
    event.preventDefault();
  }
});

$( "#password" ).keyup(function( event ) {
  if ($('#password-confirm').val() == $('#password').val()) {
    $('#submit-pass').prop('disabled', false);
    $('#p-hidden').prop('hidden', true);
  }else {
    $('#submit-pass').prop('disabled', true);
    $('#p-hidden').prop('hidden', false);
  }
}).keydown(function( event ) {
  if ( event.which == 13 ) {
    event.preventDefault();
  }
});

$('#password').click(function(){
    $('#submit-pass').prop('disabled', true);
});


</script>
@endsection

@section('title')
Editar contrato
@endsection

@section('content')
    <!-- Start content -->

te<div class="content">
    <div class="container">
      <br>
      <div class="row">

        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/organization.svg" alt="organization.svg">
              </div>
              <div class="text-right">
                  <p class="m-t-5 text-uppercase font-14 font-600">{{$empresa->razonsocial_eecc}}</p>
                  <p class="m-t-5 text-uppercase font-14 font-600">{{$contrato->emailadc_eecc}}</p>
              </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/diploma_2.svg" alt="diploma_2.svg">
              </div>
              <div class="text-right">
                  <p class="m-t-5 text-uppercase font-14 font-600">Contrato N° {{$contrato->numero_ctto}}</p>
                  <p class="m-t-5 text-uppercase font-14 font-600">{{ $compania->descripcion }}</p>
              </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6">
          <div class="card-box widget-box-three">
              <div class="bg-icon pull-left">
              <img class="" src="/images/icons/businessman.svg" alt="businessman.svg">
              </div>
              <div class="text-right">
                <p class="m-t-5 text-uppercase font-14 font-600">Correo Soporte</p>
                <p class="m-t-5 text-uppercase font-14 font-600">marioossandon@workmate.cl</p>
              </div>
          </div>
        </div>
      </div>
      @if($not == 1)
      <div class="row">
        <div class="alert alert-icon alert-info alert-dismissible fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
          <i class="mdi mdi-check-all"></i>
          <strong>Actualizar información del contacto del contrato!</strong> <br> Favor actualizar la información del contacto del contrato, esto debido a que permitirá generar futuras comunicaciones al correo designado.
        </div>
      </div>
      @endif
      @if($not == 2)
      <div class="row">
        <div class="alert alert-icon alert-success alert-dismissible fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
          <i class="mdi mdi-check-all"></i>
          <strong>Contraseña actualizada!</strong> <br> Contraseña ha sido actualizada correctamente en plataforma.
        </div>
      </div>
      @endif
      @if($not == 3)
      <div class="row">
        <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">×</span>
          </button>
          <i class="mdi mdi-check-all"></i>
          <strong>No se guardó actualización!</strong> <br> No se pudo actualizar contraseña, favor probar nuevamente, en caso que error continue favor contactar a soporte marioossandon@workmate.cl.
        </div>
      </div>
      @endif
      <div class="row">
        <div class="col-lg-6">
          <div class="card-box">
              <h4 class="header-title m-t-0">Información del contrato</h4>
              <p class="text-muted font-14 m-b-20">
                  Actualizar información sobre el ADC o contacto del contrato n° {{$contrato->numero_ctto}}
              </p>
              <form method="post" action="{!! url('/contrato/update/') !!}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>
                  <div class="form-group">
                      <label for="userName">Nombre de ADC<span class="text-danger">*</span></label>
                      <input type="text" name="adc_eecc" parsley-trigger="change" required
                             placeholder="Ingresar el nombre del contacto" class="form-control" id="adc_eecc" value="{{ $contrato->adc_eecc}}">
                  </div>
                  <div class="form-group">
                      <label for="emailAddress">Email de ADC<span class="text-danger">*</span></label>
                      <input type="email" name="emailadc_eecc" parsley-trigger="change" required
                             placeholder="Ingresar el Email del contacto" class="form-control" id="emailadc_eecc" value="{{ $contrato->emailadc_eecc}}">
                  </div>
                  <div class="form-group text-right m-b-0">
                      <button class="btn btn-primary waves-effect waves-light" type="submit">
                          Actualizar
                      </button>
                  </div>
              </form>
          </div> <!-- end card-box -->
        </div>
        <div class="col-lg-6">
          <div class="card-box">
              <h4 class="header-title m-t-0">Actualizar contraseña</h4>
              <p class="text-muted font-14 m-b-20">
                  Actualizar contraseña para acceder a la librería virtual
              </p>
              <form method="POST" action="{!! url('/update/') !!}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"></input>

                  <div class="form-group row">
                      <label for="password" class="col-md-4 col-form-label text-md-right">Nueva contraseña</label>

                      <div class="col-md-6">
                          <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                          @if ($errors->has('password'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                      </div>
                  </div>

                  <div class="form-group row">
                      <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar contraseña</label>

                      <div class="col-md-6">
                          <input id="password-confirm" type="password" class="form-control" name="password-confirm" required>
                      </div>
                      <div class="col-md-4">

                      </div>
                      <div class="col-md-8">
                        <p class="text-danger" hidden id='p-hidden' name='p-hidden'>
                            Contraseñas ingresadas no concuerdan.
                        </p>
                      </div>
                  </div>

                  <div class="form-group text-right m-b-0">
                      <button class="btn btn-primary waves-effect waves-light" type="submit" id="submit-pass" disabled>
                          Actualizar
                      </button>
                  </div>
              </form>
          </div> <!-- end card-box -->
        </div>
      </div>
      <br>
    </div> <!-- container -->
</div> <!-- content -->
@endsection
