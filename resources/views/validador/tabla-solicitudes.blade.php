
    <div class="col-sm-12">
        <div class="card-box table-responsive">
            <h4 class="m-t-0 header-title"><b>Solicitudes</b></h4>
            <p class="text-muted font-14 m-b-30">
                Se presenta listado de solicitudes.
            </p>
            <table id="datatable-buttons" class="table table-striped table-bordered">
                <thead>
                <tr>
                  <th>Nº Solicitud</th>
                  <th>Empresa</th>
                  <th>N° de contrato</th>
                  <th>Contacto</th>
                  <th>Estado</th>
                  <th>Fecha creación</th>
                  <th>Fecha Actualización</th>
                  <th>Acción</th>

                </tr>
                </thead>
                <tbody>
                @foreach($data["accesos"] as $acceso)
                <?php
                  $ctto = App\Contrato::where('id','=',$acceso->fkid_contrato)->first();
                  $emp = App\Empresa::where('id','=',$ctto->fkid_empresa)->first();
                ?>
                    <tr>

                      <th>{{$acceso->id}}</th>
                      <th>{{$emp->razonsocial_eecc}}</th>
                      <th>{{$ctto->numero_ctto}}</th>
                      <th>{{$ctto->emailadc_eecc}}</th>

                      @if($acceso->estado_solicitud == 0)
                      <td><span class="label label-info">No Enviada</span></td>
                      @endif
                      @if($acceso->estado_solicitud == 1)
                      <td><span class="label label-warning">Pendiente</span></td>
                      @endif
                      @if($acceso->estado_solicitud == 2)
                      <td><span class="label label-success">Finalizado</span></td>
                      @endif
                      @if($acceso->estado_solicitud == 3)
                      <td><span class="label label-danger">Cancelado</span></td>
                      @endif
                      <th>{{$acceso->created_at}}</th>
                      <th>{{$acceso->updated_at}}</th>
                      <td class=""><a href="{{  url('validador/revision/'. Crypt::encryptString($acceso->id)) }}" class="btn btn-custom waves-effect waves-light">Revisar solicitud</a></td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
