
    <link href="{{asset('css/font-icons/entypo/css/entypo.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="{{asset('js/datatable/jquery.dataTables.css')}}">
    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/css/neon-core.css')}}" rel="stylesheet">
    <link href="{{asset('/css/neon-theme.css')}}" rel="stylesheet">
    <link href="{{asset('/css/neon-forms.css')}}" rel="stylesheet">
    <link href="{{asset('/css/custom.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <link rel="icon" href="{{ asset('images/LogoWM.png') }}">
