@extends('layouts.login')

@section('content')
<div class="login-container">
        <div class="login-header login-caret" id ="login_c">
            <div class="login-content">
                <a href="#" class="logo">
                    <img src="{{ asset('images/logo_amsa.png') }}" width="300" alt="" />
                </a>
              <br>
              <br>
              <br>

                <p class="description"><p style="font-size:18px; color:#D5DBDB;"><b>Biblioteca Documental</b></p></p>
            </div>
        </div>

        <div class="login-progressbar">
            <div></div>
        </div>

        <div class="login-form">
        <div class="login-content">
            <br>
            @if(1 == 1)
            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-user"></i>
                        </div>
                        <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Usuario" autocomplete="off" required autofocus>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="entypo-key"></i>
                        </div>
                        <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="Contraseña" autocomplete="off" required autofocus>
                    </div>
                </div>
                @if ($errors->has('email'))
                    <div class="form-group">
                        <button id="buttonerror" type="button" class="btn btn-default btn-lg btn-block btn-icon icon-left google-button">
                            <strong>{{ $errors->first('email') }}</strong>
                            <i class="entypo-attention"></i>
                        </button>
                    </div>
                @endif
                @if ($errors->has('password'))
                    <div class="form-group">
                        <button id="buttonerror" type="button" class="btn btn-default btn-lg btn-block btn-icon icon-left google-button">
                            <strong>{{ $errors->first('password') }}</strong>
                            <i class="entypo-attention"></i>
                        </button>
                    </div>
                @endif
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block btn-login" id="loginbutton">
                        <i class="entypo-login"></i>
                        Entrar
                    </button>
                    {{--
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    --}}
                </div>
            </form>
            <br />
            @else
                <div class="form-group alert alert-danger">
                    <b>Estimado usuario, el navegador que esta utilizando no es compatible con el sistema.</b>
                    <p>Favor descargue alguno de los navegadores del listado que aparece a continuación.
                </div>
            @endif
            <div class="form-group">

                <table width="320" border="0" style="color:#FFFFFF;font-size:10px">
                    <tr>
                        <td colspan="3">Navegadores Recomendados</td>
                    </tr>
                    <tr>
                        <td width="106"><a href="https://www.mozilla.org/es-CL/firefox/new/"  target="_blank">
                                <img src="{{ asset('images/mozilla_firefox.png') }}" width="30" alt="" /></a></td>
                        <td width="106"><a href="https://www.google.com/chrome/browser/desktop/index.html" target="_blank" >
                                <img src="{{asset('images/chrome_ico.png')}}" width="30" alt="" /></a></td>
                        <td width="106"><a href="http://windows.microsoft.com/es-es/internet-explorer/download-ie"  target="_blank"><img src="{{asset('images/internet_explorer.png')}}" width="30" alt="" /></a></td>
                    </tr>
                    <tr>
                        <td>firefox</td>
                        <td>chrome</td>
                        <td>iexplorer 11 </td>
                    </tr>
                </table>
            </div>
            <div class="login-bottom-links" style="color: white">

              Correo de ayuda:  marioossandon@workmate.cl
                <br/>
                &copy; RM-Software propiedad <strong><a href="http://www.workmate.cl/">Workmate Ltda.</a></strong> 2018
            </div>
        </div>
    </div>
</div>
@endsection
