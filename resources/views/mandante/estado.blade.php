@extends('layouts.app')


@section('css')

@endsection

@section('scripts')

@endsection

@section('title')
Revisión documental
@endsection

@section('content')
<!-- Start content -->
<div class="content">
   <div class="container">
      <br>
      <div class="row">
        <a href="{!! url('/solicitud-no-enviada')!!}">
          <div class="col-lg-3 col-md-6">
            <div class="card-box widget-box-two widget-two-custom">
              <i class="dripicons-clock widget-two-icon"></i>
              <div class="wigdet-two-content">
                <p class="m-0 text-uppercase text-white font-600 text-overflow" title="Statistics">Solicitudes sin enviar</p>
                <h2 class="text-white"><span data-plugin="counterup">{{$countNoEnviada}}</span> </h2>
              </div>
            </div>
          </div><!-- end col -->
        </a>
        <a href="{!! url('/solicitud-pendiente')!!}">
          <div class="col-lg-3 col-md-6">
            <div class="card-box widget-box-two widget-two-warning">
              <i class="dripicons-archive widget-two-icon"></i>
              <div class="wigdet-two-content">
                <p class="m-0 text-white text-uppercase font-600 text-overflow" title="User Today">Solicitudes en revisión</p>
                <h2 class="text-white"><span data-plugin="counterup">{{$countPendiente}}</span> </h2>
              </div>
            </div>
          </div><!-- end col -->
        </a>
        <a href="{!! url('/solicitud-finalizada')!!}">
          <div class="col-lg-3 col-md-6">
            <div class="card-box widget-box-two widget-two-inverse">
              <i class="dripicons-checkmark widget-two-icon"></i>
              <div class="wigdet-two-content">
                <p class="m-0 text-uppercase text-white font-600 text-overflow" title="User This Month">Solicitudes finalizadas</p>
                <h2 class="text-white"><span data-plugin="counterup">{{$countFinalizada}}</span> </h2>
              </div>
            </div>
          </div><!-- end col -->
        </a>
        <a href="{!! url('/solicitudes')!!}">
          <div class="col-lg-3 col-md-6">
            <div class="card-box widget-box-two widget-two-danger">
              <i class="dripicons-warning widget-two-icon"></i>
              <div class="wigdet-two-content">
                <p class="m-0 text-white text-uppercase font-600 text-overflow" title="Request Per Minute">Listado completo</p>
                <h2 class="text-white"><span data-plugin="counterup">{{ $countSolicitudes }}</span> </h2>
              </div>
            </div>
          </div><!-- end col -->
        </a>
      </div>
      <div class="row">
         <div class="row">
            <div class="col-sm-12">
               <div class="card-box ">
                  <h4 class="m-t-0 header-title"><b>Solicitudes</b></h4>
                  <p class="text-muted font-14 m-b-30">
                     Se presenta listado de solicitudes realizadas.
                  </p>
                  <div class="table-responsive">
                     <table id="datatable-buttons" class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead>
                           <tr>
                              <th>Nº Solicitud</th>
                              <th>Empresa</th>
                              <th>N° de contrato</th>
                              <th>Estado</th>
                              <th>Compañia</th>
                              <th>Fecha creación</th>
                              <th>Fecha Actualización</th>
                              <th>Acción</th>
                           </tr>
                        </thead>
                        <tbody>
                           @foreach ($solicitudes as $key => $value)
                           <?php $ctto = App\Contrato::where('id','=',$value->fkid_contrato)->first();
                                 $emp = App\Empresa::where('id','=',$ctto->fkid_empresa)->first();
                                 $comp = App\Compania::where('id','=',$ctto->fkid_compania)->first();

                              ?>
                           <tr>
                              <th>{{$value->id}}</th>
                              <th>{{$emp->razonsocial_eecc}}</th>
                              <th>{{$ctto->numero_ctto}}</th>
                              <th>{{$value->estado_solicitud == 0 ? 'pendiente':'aprobada' }}</th>
                              <th>{{$comp->descripcion}}
                              <th>{{$value->created_at}}</th>
                              <th>{{$value->updated_at}}</th>
                              <td class=""><a href="{{  url('validador/revision/'. Crypt::encryptString($value->id)) }}" class="btn btn-info">Revisar solicitud</a></td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                     {!! $solicitudes->render() !!}
                  </div>
               </div>
            </div>
         </div>
      </div>
      <br>
   </div>
   <!-- container -->
</div>
<!-- content -->
@endsection
