@extends('layouts.app')
@section('css')
    <link rel="stylesheet" href="../plugins/jquery-datatables-editable/datatables.css" />
    <link rel="stylesheet" href="../plugins/magnific-popup/css/magnific-popup.css" />
    <link rel="stylesheet" href="../plugins/datatables/buttons.dataTables.css" />
@endsection
@section('content')
<!-- Start content -->
<div class="content">
   <div class="container" >
      <br>
      <div class="row">
        <a id ="finalizados" href="#">
       <div class="col-lg-3 col-md-6">
         <div class="card-box widget-box-two widget-two-success">
            <i class="dripicons-checkmark widget-two-icon"></i>
            <div class="wigdet-two-content">
               <p class="m-0 text-uppercase text-white font-600 text-overflow" title="User This Month">Finalizados</p>
               <h2 class="text-white"><span data-plugin="counterup">{{$countFinalizada}}</span> </h2>
            </div>
         </div>
       </div>
       </a>
        <a id ="pendientes" href="#">
         <div class="col-lg-3 col-md-6">
            <div class="card-box widget-box-two widget-two-info">
               <i class="dripicons-clock widget-two-icon"></i>
               <div class="wigdet-two-content">
                  <p class="m-0 text-uppercase text-white font-600 text-overflow" title="Statistics">No Enviada</p>
                  <h2 class="text-white"><span data-plugin="counterup">{{$countNoEnviada}}</span> </h2>
               </div>
            </div>
         </div>
       </a>
         <!-- end col -->
           <a id ="revision" href="#">
         <div class="col-lg-3 col-md-6">
            <div id="revision" class="card-box widget-box-two widget-two-warning">
               <i class="dripicons-archive widget-two-icon"></i>
               <div class="wigdet-two-content">
                  <p class="m-0 text-white text-uppercase font-600 text-overflow" title="User Today">Pedientes de Validador</p>
                  <h2 class="text-white"><span data-plugin="counterup">{{$countPendiente}}</span> </h2>
               </div>
            </div>
         </div>
         </a>
         <!-- end col -->

        <a id ="todos" href="#">
        <div class="col-lg-3 col-md-6">
           <div class="card-box widget-box-two widget-two-primary">
              <i class="dripicons-checkmark widget-two-icon"></i>
              <div class="wigdet-two-content">
                 <p class="m-0 text-uppercase text-white font-600 text-overflow" title="User This Month">Total de Solicitudes</p>
                 <h2 class="text-white"><span data-plugin="counterup">{{$countSolicitudes}}</span> </h2>
              </div>
           </div>
       </div>
       </a>

    </div><!-- end col -->
    <div class="row">
        <div class="col-md-4">
            <button id="boton_reporte" type="button" class="btn btn-custom waves-effect waves-light hidden"> <i class="fi-file-subtract"></i> <span>Generar Reporte</span> </button>
            <button id="boton_reporte_cargando" type="button" class="btn btn-custom waves-effect waves-light hidden" disabled="true"> <i class="fa fa-spin fa-spinner"></i> <span>Generando...</span> </button>
        </div>
    </div>
      </div>

          <div class="row" id="datos_reporte">

          </div>


        <br>

</div>
<!-- content -->
@endsection

@section('scripts')

    <script src="../helpers/helper.js"></script>

    <script src="../plugins/magnific-popup/js/jquery.magnific-popup.min.js"></script>
    <script src="../plugins/jquery-datatables-editable/jquery.dataTables.js"></script>
    <script src="../plugins/datatables/dataTables.bootstrap.js"></script>
    <script src="../plugins/tiny-editable/mindmup-editabletable.js"></script>
    <script src="../plugins/tiny-editable/numeric-input-example.js"></script>

    <script src="../plugins/datatables/dataTables.buttons.js"></script>

    <script src="../plugins/datatables/jszip.min.js"></script>
    <script src="../plugins/datatables/pdfmake.min.js"></script>
    <script src="../plugins/datatables/vfs_fonts.js"></script>
    <script src="../plugins/datatables/buttons.html5.js"></script>

    <script src="{!! asset('pages/jquery.datatables.editable.init.js') !!}" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready(function () {
          var estado_solicitud =2;
          $("#boton_reporte").trigger("click");
          // Cargar solicitudes pendientes
          $("#pendientes").click(function(){
            estado_solicitud = 0;
            $("#boton_reporte").trigger("click");
            });
            // Cargar solicitudes en revision
          $("#revision").click(function(){
              estado_solicitud = 1;
              $("#boton_reporte").trigger("click");
            });
            // Cargar solicitudes finalizadas
          $("#finalizados").click(function(){
              estado_solicitud = 2;
              $("#boton_reporte").trigger("click");
            });
            // Cargar todas las solicitudes
            $("#todos").click(function(){
                estado_solicitud = 3;
                $("#boton_reporte").trigger("click");
              });

            //generar reporte al cargar
            $("#boton_reporte").ready(function(){

                $.ajax({
                    url: '/tabla-solicitudes',
                    type: 'GET',
                    data: {
                        estado_solicitud: estado_solicitud

                    }

                }).done(function(response){

                    $("#datos_reporte").html(response);

                    $("#datatable-buttons").DataTable({
                      "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                      },
                        dom: "Bfrtip",
                        buttons: [{
                            extend: "excel",
                            text: 'Exportar a Excel',
                            className: "btn-sm"
                        }, {
                            extend: "pdf",
                            text: 'Exportar a PDF',
                            className: "btn-sm"
                        }, {
                            extend: "print",
                            text: 'Imprimir Reporte',
                            className: "btn-sm"
                        }],
                        "order": [[ 6, "desc" ]]
                    });
                });
            });


            // Generar reporte al hacer click
            $("#boton_reporte").click(function(){
                $.ajax({
                    url: '/tabla-solicitudes',
                    type: 'GET',
                    data: {
                        estado_solicitud: estado_solicitud

                    }

                }).done(function(response){

                    $("#datos_reporte").html(response);

                    $("#datatable-buttons").DataTable({
                      "language": {
                        "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                      },
                        dom: "Bfrtip",
                        buttons: [{
                            extend: "excel",
                            text: 'Exportar a Excel',
                            className: "btn-sm"
                        }, {
                            extend: "pdf",
                            text: 'Exportar a PDF',
                            className: "btn-sm"
                        }, {
                            extend: "print",
                            text: 'Imprimir Reporte',
                            className: "btn-sm"
                        }],
                        "order": [[ 6, "desc" ]]
                    });
                });
            });


        });
    </script>

@endsection
