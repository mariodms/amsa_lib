<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_perfil extends Model
{
  protected $table = 'tipo_perfil';

  protected $primaryKey = 'id';

  protected $fillable = [
    'id',
    'descripcion',
  ];

  public function perfilUser(){
    return $this->hasMany(User::class);
  }
}
