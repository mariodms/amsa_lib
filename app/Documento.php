<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Documento extends Model
{
  protected $table = 'documento';

  protected $primaryKey = 'id';

  protected $fillable = [
    'id',
    'fkid_tipo',
    'fkid_solicitud',
    'sesion',
    'url',
    'original_name',
    'created_at',
    'updated_at',
    'deleted_at',
    'deleted',
  ];

  public function solicitud(){
    return $this->belongsTo(Solicitud::class,'fkid_solicitud');
  }

  public function empresa(){
    return $this->belongsTo(Empresa::class,'fkid_compania');
  }

  public function user(){
    return $this-belongsTo(Users::class,'sesion');
  }

  public function tipoDoc(){
    return $this->belongsTo(Tipo_doc::class);
  }
}
