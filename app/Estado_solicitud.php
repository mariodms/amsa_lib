<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Solicitud;

class Estado_solicitud extends Model
{
  protected $table = 'estado_solicitud';

  protected $primaryKey = 'id';

  protected $fillable = [
    'id',
    'descripcion',
  ];

  public function solicitud(){
    return $this->hasMany(Solicitud::class);
  }

}
