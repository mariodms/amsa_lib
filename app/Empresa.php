<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
  protected $table = 'empresa';

  protected $primaryKey = 'id';

  protected $fillable = [
    'id',
    'razonsocial_eecc',
  ];

  public function contratos(){
    return $this->hasMany(Contratos::class);
  }


  protected $guarded = [];
}
