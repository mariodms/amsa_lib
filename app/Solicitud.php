<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
  protected $table = 'solicitud';

  protected $primaryKey = 'id';

  protected $fillable = [
    'id',
    'estado_solicitud',
    'edit',
    'sesion',
    'fkid_contrato',
  ];

  public function contrato(){
    return $this->belongsTo(Contratos::class,'fkid_contrato');
  }

  public function documento(){
    return $this->hasMany(Documento::class);
  }

  public function user(){
    return $this->belongsTo(User::class,'sesion');
  }

  public function estado_solicitud(){
    return $this->belongsTo(Estado_solicitud::class,'estado_solicitud');
  }
}
