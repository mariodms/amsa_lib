<?php

namespace App;

use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','fkid_perfil','fkid_empresa','user_nombre','user_apellido'
    ];

    public function empresa(){
      return $this->belongsTo(Empresa::class,'fkid_empresa');
    }

    public function solicitud(){
      return $this->hasOne(Solicitud::class,'fkid_solicitud');
    }

    public function contrato(){
      return $this->belongsTo(Contrato::class,'fkid_contrato');
    }


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
