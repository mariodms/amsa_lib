<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Solicitud;
use App\Contrato;
use App\Documento;
use App\Empresa;

class checkDirectory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkDirectory:crear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crea directorios faltantes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->photos_path = public_path('946');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $empresas = Empresa::all();
      $contratos = Contrato::all();

      foreach ($empresas as $key => $empresa) {
        $rutaEmpresa = $this->photos_path.'/'.$empresa->id;
        //$ruta = $rutaEmpresa.'/'.$contrato->id;
        //$route = '946/'.$empresa->id.'/'.$contrato->id;

        //Consulta si carpeta empresa está creada
        if (!is_dir($rutaEmpresa)) {
            mkdir($rutaEmpresa, 0777, true);
            \Log::info('Se Crea carpeta '.$empresa->id.' correspondiente a '.$empresa->razon_social);
            //Consulta si está creada la carpeta de contrato
           // if (!is_dir($ruta)) {
             //   \Log::info('Se Crea carpeta '.$contrato->id.' correspondiente a '.$contrato->numero_ctto);
                //mkdir($ruta, 0775, true);
               // \Log::info('Se ha generado la ruta '.$ruta);
            //}
        }
      }
      foreach ($empresas as $key => $empresa) {
        $contratos = Contrato::where('fkid_empresa','=',$empresa->id)->get();
        foreach ($contratos as $key => $contrato) {
            $rutaEmpresa = $this->photos_path.'/'.$empresa->id;
            $ruta = $rutaEmpresa.'/'.$contrato->id;
            $route = $this->photos_path.'/'.$empresa->id.'/'.$contrato->id;
            //Consulta si carpeta empresa está creada
                //Consulta si está creada la carpeta de contrato
               if (!is_dir($route)) {
                   \Log::info('Se Crea carpeta '.$contrato->id.' correspondiente a '.$contrato->numero_ctto);
                   mkdir($ruta, 0777, true);
                }
            }
        }
    }
}
