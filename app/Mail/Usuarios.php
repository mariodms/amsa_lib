<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use App\Contrato
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Usuarios extends Mailable
{
    use Queueable, SerializesModels;
    public $distressCall;


    public function __construct(DistressCall $distressCall)
      {
          $this->distressCall = $distressCall;
      }


    /**
     * Build the message.
     *
     * @return $this
     */
     public function build()
     {
       $emails = Contrato::where('emailadc_eecc','<>','')-get();
         return $this->view('mails.usuarios',compact('emails'));
     }
}
