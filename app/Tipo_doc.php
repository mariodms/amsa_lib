<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipo_doc extends Model
{
  protected $table = 'tipo_doc';

  protected $primaryKey = 'id';

  protected $fillable = [
    'id',
    'descripcion',
    'especial'
  ];

  public function documento(){
    return $this->hasMany(Documentos::class);
  }

}
