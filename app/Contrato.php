<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contrato extends Model
{
    protected $table = 'contrato';

    protected $primaryKey = 'id';

    protected $fillable = [
      'id',
      'fkid_empresa',
      'numero_ctto',
      'adc_eecc',
      'emailadc_eecc',
      'gesto_ctto',
      'desempeno_ctto',
      'actualizado_ctto',
      'fkid_compania',
      'create_at',
      'update_at',
    ];

    public function compania(){
      return $this->belongsTo(Compania::class,'fkid_compania');
    }

    public function empresa(){
      return $this->belongsTo(Empresa::class,'fkid_compania');
    }

    public function solicitud(){
      return $this->hasMany(Solicitud::class);
    }

    public function user(){
      return $this->hasMany(User::class);
    }

    public function getCtto($id){
      $var = Contrato::where('id','=',$id);
      return $var->numero_ctto;
    }
}
