<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Empresa;
use App\Contrato;
use App\Tipo_doc;
use App\Compania;
use App\Solicitud;
use App\Documento;
use \Crypt;

//Estados de la solicitud
// 0 = no enviada
// 1 = pendiente
// 2 = finalizada
// 3 = bloqueada

class SolicitudController extends Controller
{
    public function create(){
        if(Auth::user()->fkid_perfil == "1" ){
          $contrato = Contrato::find(Auth::user()->fkid_contrato);
           $empresa = Empresa::find($contrato->fkid_empresa);
           $solicitud = Solicitud::where('sesion',Auth::user()->id)->first();
           $compania = Compania::find($contrato->fkid_compania);

           if($contrato->actualizado_ctto == 0){
             $not = 1;
             return View('contrato.edit',compact('contrato','empresa','compania','not'));
           }

           if($contrato->gesto_ctto == 1 && $contrato->desempeno_ctto == 1 ){
             $tipo_doc = Tipo_doc::whereNotIn('id',[6])->get();
           }elseif($contrato->gesto_ctto == 1 && $contrato->desempeno_ctto == 0) {
             $tipo_doc = Tipo_doc::whereNotIn('id',[5,6])->get();
           }elseif($contrato->gesto_ctto == 0 && $contrato->desempeno_ctto == 1){
             $tipo_doc = Tipo_doc::whereNotIn('id',[4,6])->get();
           }else {
             $tipo_doc = Tipo_doc::whereNotIn('id',[4,5,6])->get();
           }

           // se consulta si existe solicitud, en caso que no la crea
           if($solicitud == null ){
             \Log::info("No posee solicitud");
             \Log::info($contrato->id);
             $solicitud = new Solicitud();
             $solicitud->estado_solicitud = 0;
             $solicitud->sesion = Auth::user()->id;
             $solicitud->fkid_contrato = $contrato->id;
             $solicitud->save();
           }

           \Log::info($solicitud);
           $estado = $solicitud->estado_solicitud;
           $id_solicitud = $solicitud->id;
           $razon_social = $empresa->razonsocial_eecc;
           $adc_eecc = $contrato->adc_eecc;
           $n_contrato = $contrato->numero_ctto;
           $email_adc = $contrato->emailadc_eecc;
           // se buscan docunetos en db para poder generar vista si es que existen
           $documentos = Documento::where('fkid_solicitud','=',$solicitud->id)->where('deleted','=','0')->where('fkid_tipo','<>',6)->orderBy('fkid_tipo')->get();
           \Log::info($solicitud->edit." ".$solicitud->estado_solicitud);
            if($solicitud->estado_solicitud == 1 || $solicitud->estado_solicitud == 2 || $solicitud->estado_solicitud == 3 || $solicitud->edit == 0){
              $not = 0;
              return view('eecc.estatus', compact('not','solicitud','contrato','empresa','compania','email_adc','adc_eecc','not','documentos'));
            }else{
              return view('eecc.index',compact('id_solicitud','solicitud','estado','razon_social','empresa','contrato','adc_eecc','n_contrato','email_adc','documentos','tipo_doc','compania' ));
            }
        }
        elseif (Auth::user()->fkid_perfil == "2" ) {
          $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
          $countPendiente = $solicitudPendiente->count();
          $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
          $countFinalizada = $solicitudFinalizada->count();
          $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
          $countNoEnviada = $solicitudNoEnviada->count();
          $solicitudes = Solicitud::where('estado_solicitud' ,'=',1)->get();
          $solicitudesTodas = Solicitud::all();
          $countSolicitudes = $solicitudesTodas->count();
          $contrato = new Contrato();
          $not = 0;

                    return view('validador.index', compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes','solicitudes','contrato','not'));
          }else {
            $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
            $countPendiente = $solicitudPendiente->count();
            $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
            $countFinalizada = $solicitudFinalizada->count();
            $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
            $countNoEnviada = $solicitudNoEnviada->count();
            $solicitudes = Solicitud::where('estado_solicitud' ,'=',1)->get();
            $solicitudesTodas = Solicitud::all();
            $countSolicitudes = $solicitudesTodas->count();
          $not = 0;

          return view('mandante.index',compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes','countRevision','not'));
        }
    }

    public function edit($id){

      $var = Crypt::decryptString($id);
      $solicitud = Solicitud::find($var);
      $documentos = Documento::where('fkid_solicitud','=',$var)->where('deleted','=','0')->orderBy('fkid_tipo')->get();
      $contrato =Contrato::find($solicitud->fkid_contrato);
      $empresa = Empresa::find($contrato->fkid_empresa);
      $compania = Compania::find($contrato->fkid_compania);

      \Log::info('Sol '.$solicitud->id.' ctto '.$contrato->id.' emp '.$empresa->id.' com '.$compania->id);

      return view('solicitud.validador',compact('solicitud','documentos','contrato','empresa','compania'));
    }
    public function tablasolicitudes(){

            $estado_solicitud = $_GET["estado_solicitud"];
            \Log::info('estado solicitud' . $estado_solicitud);

            if($estado_solicitud == 3){
              $solicitudes = Solicitud::all();
            }
            else{
              $solicitudes = Solicitud::where('estado_solicitud' ,'=',$estado_solicitud)->get();
            }

          $data = array(
              "accesos" => $solicitudes
          );
          return view('validador.tabla-solicitudes')->with('data',$data);
      }

    public function noEnviadas(){
      $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
      $countNoEnviada = $solicitudNoEnviada->count();
      $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
      $countPendiente = $solicitudPendiente->count();
      $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
      $countFinalizada = $solicitudPendiente->count();
      $solicitudes = Solicitud::all();
      $countSolicitudes = $solicitudes->count();
      $solicitudes = Solicitud::where('estado_solicitud','=',0)->paginate(10);
      return view('mandante.estado',compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes','solicitudes'));
    }

    public function pendientes(){
      $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
      $countNoEnviada = $solicitudNoEnviada->count();
      $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
      $countPendiente = $solicitudPendiente->count();
      $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
      $countFinalizada = $solicitudPendiente->count();
      $solicitudes = Solicitud::all();
      $countSolicitudes = $solicitudes->count();
      $solicitudes = Solicitud::where('estado_solicitud','=',1)->paginate(10);
      return view('mandante.estado',compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes','solicitudes'));
    }

    public function finalizadas(){
      $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
      $countNoEnviada = $solicitudNoEnviada->count();
      $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
      $countPendiente = $solicitudPendiente->count();
      $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
      $countFinalizada = $solicitudPendiente->count();
      $solicitudes = Solicitud::all();
      $countSolicitudes = $solicitudes->count();
      $solicitudes = Solicitud::where('estado_solicitud','=',2)->paginate(10);
      return view('mandante.estado',compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes','solicitudes'));
    }

    public function index(){
      $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
      $countNoEnviada = $solicitudNoEnviada->count();
      $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
      $countPendiente = $solicitudPendiente->count();
      $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
      $countFinalizada = $solicitudPendiente->count();
      $solicitudes = Solicitud::all();
      $countSolicitudes = $solicitudes->count();
      $solicitudes = Solicitud::whereIn('estado_solicitud',[1,2,0])->paginate(10);
      return view('mandante.estado',compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes','solicitudes'));
    }

    public function enviar($id){
      $var = Crypt::decryptString($id);
      $solicitud = Solicitud::find($var);
      $contrato = Contrato::find($solicitud->fkid_contrato);
      $empresa = Empresa::find($contrato->fkid_empresa);
      $compania = Compania::find($contrato->fkid_compania);
      $solicitud->estado_solicitud = 1; // Solicitud pendiente
      $documentos = Documento::where('fkid_solicitud','=',$solicitud->id)->where('deleted','=','0')->where('fkid_tipo','<>',6)->orderBy('fkid_tipo')->get();
      if($solicitud->save()){
        $not = 1;
        return view('eecc.estatus', compact('not','solicitud','contrato','empresa','compania','documentos'));
      }else {
        $not = 0;
        return redirect()->action('SolicitudController@create');
      }
    }

    public function change($id){
      $var = Crypt::decryptString($id);
      if(Auth::user()->fkid_perfil == "1" ){
        $solicitud = Solicitud::find($var);
        \Log::info($var);
        $contrato = Contrato::find($solicitud->fkid_contrato);
        $empresa = Empresa::find($contrato->fkid_empresa);
        \Log::info($empresa);

        if($contrato->gesto_ctto == 1 && $contrato->desempeno_ctto == 1 ){
          $tipo_doc = Tipo_doc::whereNotIn('id',[6])->get();
        }elseif($contrato->gesto_ctto == 1 && $contrato->desempeno_ctto == 0) {
          $tipo_doc = Tipo_doc::whereNotIn('id',[5,6])->get();
        }elseif($contrato->gesto_ctto == 0 && $contrato->desempeno_ctto == 1){
          $tipo_doc = Tipo_doc::whereNotIn('id',[4,6])->get();
        }else {
          $tipo_doc = Tipo_doc::whereNotIn('id',[4,5,6])->get();
        }

        if($solicitud == null ){
          \Log::info("No posee solicitud");
          \Log::info($contrato->id);
          $solicitud = new Solicitud();
          $solicitud->estado_solicitud = 0;
          $solicitud->sesion = Auth::user()->id;
          $solicitud->fkid_contrato = $contrato->id;
          $solicitud->save();
        }
        // se consulta si existe solicitud, en caso que no la crea
        \Log::info($solicitud);
        $estado = $solicitud->estado_solicitud;
        $id_solicitud = $solicitud->id;
        $razon_social = $empresa->razonsocial_eecc;
        $adc_eecc = $contrato->adc_eecc;
        $n_contrato = $contrato->numero_ctto;
        $email_adc = $contrato->emailadc_eecc;
        // se buscan docunetos en db para poder generar vista si es que existen
        $documentos = Documento::where('fkid_solicitud','=',$solicitud->id)->where('deleted','=','0')->where('fkid_tipo','<>',6)->orderBy('fkid_tipo')->get();
        $compania = Compania::find($contrato->fkid_compania);
        \Log::info('Estado solicitud = ' . $estado);
        return view('eecc.index',compact('id_solicitud','estado','razon_social','adc_eecc','n_contrato','email_adc','documentos','tipo_doc','compania','empresa','contrato' ));
      }
      elseif (Auth::user()->fkid_perfil == "2" ) {
        $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
        $countPendiente = $solicitudPendiente->count();
        $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
        $countFinalizada = $solicitudFinalizada->count();
        $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
        $countNoEnviada = $solicitudNoEnviada->count();
        $solicitudes = Solicitud::where('estado_solicitud' ,'=',1)->get();
        $contrato = new Contrato();
        $not = 0;

        return view('validador.index', compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes','solicitudes','contrato','not'));
      }else {
        $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
        $countPendiente = $solicitudPendiente->count();
        $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
        $countFinalizada = $solicitudFinalizada->count();
        $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
        $countNoEnviada = $solicitudNoEnviada->count();
        $solicitudes = Solicitud::all();
        $countSolicitudes = $solicitudes->count();

        return view('mandante.index',compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes'));
      }
    }



}
