<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Crypt;
use App\Contrato;
use App\Empresa;
use App\Compania;
use Illuminate\Support\Facades\Auth;


class ContratoController extends Controller
{
    public function edit($id){
      $var = Crypt::decryptString($id);
      $contrato = Contrato::find($var);
      $empresa = Empresa::find($contrato->fkid_empresa);
      $compania = Compania::find($contrato->fkid_compania);
      $not = 0;

      return View('contrato.edit',compact('contrato','empresa','compania','not'));

    }

    public function update(Request $request){
      $contrato = Contrato::find(Auth::user()->fkid_contrato);
      $contrato->adc_eecc = request('adc_eecc');
      $contrato->emailadc_eecc = request('emailadc_eecc');
      $contrato->actualizado_ctto = 1;
      $contrato->save();
      return redirect()->action('SolicitudController@create');
    }
}
