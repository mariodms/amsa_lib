<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Empresa;
use App\Contrato;
use App\Tipo_doc;
use App\Compania;
use App\Solicitud;
use App\Documento;
use \Crypt;

class ValidadorController extends Controller
{
  public function validar($id){
    \Log::info('entro');
    $var = Crypt::decryptString($id);
    $solicitud = Solicitud::find($var);
    $contrato = Contrato::find($solicitud->fkid_contrato);
    $empresa = Empresa::find($contrato->fkid_empresa);
    $compania = Compania::find($contrato->fkid_compania);
    $solicitud->estado_solicitud = 2; // Solicitud pendiente
    $documentos = Documento::where('fkid_solicitud','=',$solicitud->id)->where('deleted','=','0')->where('fkid_tipo','<>',6)->orderBy('fkid_tipo')->get();
    if($solicitud->save()){
      $not = 1;
      $solicitudPendiente = Solicitud::where('estado_solicitud','=',1)->get();
      $countPendiente = $solicitudPendiente->count();
      $solicitudFinalizada = Solicitud::where('estado_solicitud','=',2)->get();
      $countFinalizada = $solicitudFinalizada->count();
      $solicitudes = Solicitud::where('estado_solicitud' ,'=',1)->get();
      $solicitudNoEnviada = Solicitud::where('estado_solicitud','=',0)->get();
      $countNoEnviada = $solicitudNoEnviada->count();
      $solicitudesTodas = Solicitud::all();
      $countSolicitudes = $solicitudesTodas->count();
      $contrato = new Contrato();
      return view('validador.index', compact('countPendiente','countFinalizada','countNoEnviada','countSolicitudes','solicitudes','contrato','not'));
    }else {
      $not = 0;
      return redirect()->action('SolicitudController@create');
    }
  }
}
