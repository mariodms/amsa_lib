<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Crypt;
use App\Solicitud;
use App\Contrato;
use App\Documento;
use App\Empresa;

class DocumentoController extends Controller
{
  private $photos_path;

  public function __construct()
  {
      $this->photos_path = public_path('946');
  }

  public function checkDirectory(){
     $empresas = Empresa::all();
     $contratos = Contrato::all();

     foreach ($empresas as $key => $empresa) {
       $rutaEmpresa = $this->photos_path.'/'.$empresa->id;
       //$ruta = $rutaEmpresa.'/'.$contrato->id;
       //$route = '946/'.$empresa->id.'/'.$contrato->id;

       //Consulta si carpeta empresa está creada
       if (!is_dir($rutaEmpresa)) {
           mkdir($rutaEmpresa, 0775, true);
           \Log::info('Se Crea carpeta '.$empresa->id.' correspondiente a '.$empresa->razon_social);
       }
     }
   }

   public function checkChildDirectory(){
     $empresas = Empresa::all();
     \Log::info('ingreso');
     foreach ($empresas as $key => $empresa) {
       \Log::info('carga empresa');
       $contratos = Contrato::where('fkid_empresa','=',$empresa->id)->get();
       foreach ($contratos as $key => $contrato) {
         \Log::info('crga contrato');
           $rutaEmpresa = $this->photos_path.'/'.$empresa->id;
           \Log::info($rutaEmpresa);
           $ruta = $rutaEmpresa.'/'.$contrato->id;
           $route = '946/'.$empresa->id.'/'.$contrato->id;
           \Log::info($route);
           //Consulta si carpeta empresa está creada
               //Consulta si está creada la carpeta de contrato
              if (!is_dir($route)) {
                  \Log::info('Se Crea carpeta '.$contrato->id.' correspondiente a '.$contrato->numero_ctto);
                  mkdir($ruta, 0775, true);
               }
           }
       }
   }





    public function document_exist(Request $request){
      $var = crypt::decryptString($request->id_sol);

      $solicitud = Solicitud::find($var);
      $documentos = Documento::where('fkid_solicitud','=',$var)->where('deleted','=',0)->where('fkid_tipo','<>',6)->get();
      $contrato = Contrato::find($solicitud->fkid_contrato);
      $countDocumento = $documentos->count();
      \Log::info($countDocumento);

      if ($contrato->gesto_ctto && $contrato->desempeno_ctto ) {
        \Log::info('Especial');
        if($countDocumento == 5 ){
          \Log::info('no faltan doc');
          return '1';
        }else {
          \Log::info('faltan doc');
          return '0';
        }
      }elseif($contrato->gesto_ctto || $contrato->desempeno_ctto ){
        \Log::info('Especial');
        if($countDocumento == 4 ){
          \Log::info('no faltan doc');
          return '1';
        }else {
          \Log::info('faltan doc');
          return '0';
        }
      }else {
        if($countDocumento == 3 ){ // empresas sin bonos
          \Log::info('no faltan doc');
          return '1';
        }else {
          \Log::info('faltan doc');
          return '0';
        }
      }
    }
    public function document_exist_validador(Request $request){
      $var = crypt::decryptString($request->id_sol);

      $solicitud = Solicitud::find($var);
      $documentos = Documento::where('fkid_solicitud','=',$var)->where('deleted','=',0)->where('fkid_tipo','=',6)->get();
      $contrato = Contrato::find($solicitud->fkid_contrato);
      $countDocumento = $documentos->count();
      \Log::info($countDocumento);
      if($countDocumento <> 0 ){
        \Log::info('no faltan doc');
        return '1';
      }else {
        \Log::info('faltan doc');
        return '0';
      }
    }



}
