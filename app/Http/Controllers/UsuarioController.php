<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Crypt;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Contrato;
use App\Empresa;
use App\Compania;


class UsuarioController extends Controller
{
    public function update(Request $request){
      $contrato = Contrato::find(Auth::user()->fkid_contrato);
      $empresa = Empresa::find($contrato->fkid_empresa);
      $compania = Compania::find($contrato->fkid_compania);

      $var = \Hash::make(request('password'));
      $user = User::find(Auth::user()->id);
      $user->password = $var;

      if($user->save()){
        $not = 2;
      }else {
        $not= 3;
      }
      return View('contrato.edit',compact('contrato','empresa','compania','not'));
   }
}
