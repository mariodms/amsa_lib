<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Solicitud;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $userId = Auth::id();
        if(Auth::user()->fkid_perfil == "1" ){
          $solicitud = new Solicitud;
          return view('eecc/index');
        }
        elseif (Auth::user()->fkid_perfil == "2" ) {
          return view('solicitud/index');
        }


    }
}
