<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;
use App\Contrato;
use App\User;
use DB;



class MailController extends Controller
{
    public function store(Request $request){

        Mail::send('mails.usuarios',$request->all(), function($msj){
          $msj->subject('Usuario y contraseña AMSA Biblioteca Virtual');
          $msj->to(request('email'));
          // $msj->to('a.tamayo.web@gmail.com');
        });
        $iduser=request('id_user');
        $usuario = User::find($iduser);
        // dd($usuario);
        $usuario->user_apellido =1;
        $usuario->save();


        Session::flash('success','Correo enviado correctamente');
        return Redirect::to('/mails/index');
      }



  public function index(){
    $usuarios=User::all();
    $contrato = Contrato::find(1);
    $data = array(
        "usuarios" => $usuarios

    );
        return view('mails.index',compact('data','contrato'));
  }
}
