<?php

namespace App\Http\Controllers;

use App\Documento;
use App\Empresa;
use App\Contrato;
use App\Solicitud;
use Illuminate\Support\Facades\Auth;
use \Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\Facades\Image as Image;
use Carbon\Carbon;


class UploadImagesController extends Controller
{
    private $photos_path;

    public function __construct()
    {
        $this->photos_path = public_path('946');
    }

    /**
     * Display all of the images.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Upload::all();
        return view('uploaded-images', compact('photos'));
    }

    /**
     * Show the form for creating uploading new images.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $propiedades = new Propiedad;
        $ciudad = Ciudad::where("id", ">", "0")->orderBy('name', 'ASC')->get();
        return view('propiedades/test', compact('ciudad', 'propiedades'));
    }

    /**
     * Saving images uploaded through XHR Request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $tipo = 1;
      \Log::info($tipo);
      $this->cargar($request,$tipo);
    }
    public function store1(Request $request)
    {
      $tipo = 2;
      \Log::info($tipo);
      $this->cargar($request,$tipo);
    }
    public function store2(Request $request)
    {
      $tipo = 3;
      \Log::info($tipo);
      $this->cargar($request,$tipo);
    }
    public function store3(Request $request)
    {
      $tipo = 4;
      \Log::info($tipo);
      $this->cargar($request,$tipo);
    }
    public function store4(Request $request)
    {
      $tipo = 5;
      \Log::info($tipo);
      $this->cargar($request,$tipo);
    }

    public function store5(Request $request, $id)
    {
      $var = Crypt::decryptString($id);
      $documentos = $request->file('file');
      $solicitud = Solicitud::find($var);
      $contrato =  Contrato::find($solicitud->fkid_contrato);
      \Log::info('contrato'.$contrato->id);
      \Log::info('contrato empresa '.$contrato->fkid_empresa);
      $empresa = Empresa::find($contrato->fkid_empresa);
      \Log::info('Empresa'.$empresa->id);
      $tipo = 6;
      if (!is_array($documentos)) {
          $documentos = [$documentos];
      }
      $rutaEmpresa = $this->photos_path.'/'.$empresa->id;
      $ruta = $rutaEmpresa.'/'.$contrato->id;
      $route = '946/'.$empresa->id.'/'.$contrato->id;

      $docExisten = Documento::where('fkid_solicitud','=',$solicitud->id)->where('fkid_tipo','=',$tipo)->get();

      \Log::info($docExisten);

      foreach ($docExisten as $key => $value) {
        $doc = Documento::find($value->id);
        $doc->deleted = 1;
        $doc->deleted_at =Carbon::now()->toDateTimeString();
        $doc->save();
      }

      //Consulta si carpeta empresa está creada
      if (!is_dir($rutaEmpresa)) {
          mkdir($rutaEmpresa, 0777, true);
          \Log::info('Se Crea carpeta '.$empresa->id.' correspondiente a '.$empresa->razon_social);
          //Consulta si está creada la carpeta de contrato
          if (!is_dir($ruta)) {
              \Log::info('Se Crea carpeta '.$contrato->id.' correspondiente a '.$contrato->numero_ctto);
              mkdir($ruta, 0777, true);
          }
      }
      for ($i = 0; $i < count($documentos); $i++) {
          $documento = $documentos[$i];
          $name = sha1(date('YmdHis') . str_random(30));
          $save_name = $contrato->numero_ctto.'doc'.$tipo.'a'.$name. '.' . $documento->getClientOriginalExtension();
          $documento->move($ruta, $save_name);
          $upload = new Documento();
          $upload->fkid_tipo = $tipo;
          $upload->fkid_solicitud = $solicitud->id;
          $upload->sesion = Auth::user()->id;
          $upload->url = $route.'/'.$save_name;
          $upload->original_name = basename($documento->getClientOriginalName());
          \Log::info($tipo);
          //
          //$upload->resized_name = $resize_name;
          //$upload->original_name = basename($documento->getClientOriginalName());
          if ($upload->save()) {
              \Log::info('Documento guardada');
          } else {
              \Log::info('No pasó nada');
          }
      }
      return Response::json([
        'message' => 'Documento guardado correctamente'
      ], 200);

      //$this->cargar($request,$tipo);
    }

    public function cargar($request,$tipo){
      $documentos = $request->file('file');
      $solicitud = Solicitud::where('sesion', Auth::user()->id)->first();
      $contrato =  Contrato::find($solicitud->fkid_contrato);
      \Log::info('contrato'.$contrato->id);
      \Log::info('contrato empresa '.$contrato->fkid_empresa);
      $empresa = Empresa::find($contrato->fkid_empresa);
      \Log::info('Empresa'.$empresa->id);
      $fkidtipo = $tipo;
      if (!is_array($documentos)) {
          $documentos = [$documentos];
      }
      $rutaEmpresa = $this->photos_path.'/'.$empresa->id;
      $ruta = $rutaEmpresa.'/'.$contrato->id;
      $route = '946/'.$empresa->id.'/'.$contrato->id;

      $docExisten = Documento::where('fkid_solicitud','=',$solicitud->id)->where('fkid_tipo','=',$fkidtipo)->get();

      \Log::info($docExisten);

      foreach ($docExisten as $key => $value) {
        $doc = Documento::find($value->id);
        $doc->deleted = 1;
        $doc->deleted_at =Carbon::now()->toDateTimeString();
        $doc->save();
      }

      //Consulta si carpeta empresa está creada
      if (!is_dir($rutaEmpresa)) {
          mkdir($rutaEmpresa, 777, true);
          \Log::info('Se Crea carpeta '.$empresa->id.' correspondiente a '.$empresa->razon_social);
          //Consulta si está creada la carpeta de contrato
          if (!is_dir($ruta)) {
              \Log::info('Se Crea carpeta '.$contrato->id.' correspondiente a '.$contrato->numero_ctto);
              mkdir($ruta, 777, true);
          }
      }
      for ($i = 0; $i < count($documentos); $i++) {
          $documento = $documentos[$i];
          $name = sha1(date('YmdHis') . str_random(30));
          $save_name = $contrato->numero_ctto.'doc'.$tipo.'a'.$name. '.' . $documento->getClientOriginalExtension();
          $documento->move($ruta, $save_name);
          $upload = new Documento();
          $upload->fkid_tipo = $fkidtipo;
          $upload->fkid_solicitud = $solicitud->id;
          $upload->sesion = Auth::user()->id;
          $upload->url = $route.'/'.$save_name;
          $upload->original_name = basename($documento->getClientOriginalName());
          \Log::info($fkidtipo);
          //
          //$upload->resized_name = $resize_name;
          //$upload->original_name = basename($documento->getClientOriginalName());
          if ($upload->save()) {
              \Log::info('Documento guardada');
          } else {
              \Log::info('No pasó nada');
          }
      }
      return Response::json([
        'message' => 'Documento guardado correctamente'
      ], 200);
    }

    /**
     * Remove the images from the storage.
     *
     * @param Request $request
     */
    public function destroy(Request $request)
    {
      $documentos = $request->file('file');
      $solicitud = Solicitud::where('sesion', Auth::user()->id)->first();
      $contrato =  Contrato::find($solicitud->fkid_contrato);
      $empresa = Empresa::find($contrato->fkid_empresa);
      $rutaEmpresa = $this->photos_path.'/'.$empresa->id;
      $ruta = $rutaEmpresa.'/'.$contrato->id;

        \Log::info('Doc eliminado');
        $filename = $request->id;
        $uploaded_image = Documento::where('original_name', basename($filename))->first();

        if (empty($uploaded_image)) {
            return Response::json(['message' => 'Documento seleccionado ya no existe'], 400);
        }

        $file_path = $ruta . '/' . $uploaded_image->url;
        $permisos = substr(sprintf('%o', fileperms($file_path)), -4);

        \Log::info($file_path);

        if (file_exists($file_path)) {
            unlink($file_path);
            \Log::info('Se elimina documento de '.$uploaded_image->id_propiedad);
        }

        if (file_exists($resized_file)) {
            unlink($resized_file);
        }

        if (!empty($uploaded_image)) {
            $uploaded_image->delete();
        }

        return Response::json(['message' => 'Imagen eliminada'], 200);
    }

    public function download($id){
      \Log::info($id);
      $var = Crypt::decryptString($id);
      $documento = Documento::find($var);
      $stored = public_path().'/'.$documento->url;
      return Response::download($stored);
      \Log::info($stored);
    }


}
