<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compania extends Model
{
    protected $table = 'compania';

    protected $primaryKey = 'id';

    protected $fillable = [
      'id',
      'descripcion',
    ];

    public function contratos(){
      return $this->hasMany(Contrato::class);
    }

    protected $guarded = [];
}
