<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::post('/update','UsuarioController@update');

Route::get('/logout', function (){
    Auth::logout();

    return redirect('/');
});

Route::get('/', function () {
  \Log::info("inicio de sesion");
  if(Auth::check()){
    \Log::info("usuario con sesion creado");
    return redirect()->action('SolicitudController@create');
  }else {
    \Log::info("usuario sin sesion creada");
    return view('auth/login');
  }
});

Route::get('/home', function (){
  \Log::info("inicio de sesion");
  if(Auth::check()){
    \Log::info("usuario con sesion creado");
    return redirect()->action('SolicitudController@create');
  }else {
    \Log::info("usuario sin sesion creada");
    return view('auth/login');
  }
})->name('home');

/* Rutas de solicitud  */

Route::get('/solicitud/index','SolicitudController@create')->name('solicitudes');
Route::get('/solicitud/index','SolicitudController@index');
Route::get('/eecc/index','SolicitudController@create');;
route::get('validador/revision/{id}', 'SolicitudController@edit');
Route::get('/prueba','PruebaController@form');
Route::post('/document_exist','DocumentoController@document_exist');
Route::post('/document_exist/validador','DocumentoController@document_exist_validador');
Route::get('/enviar-solicitud/{id}','SolicitudController@enviar')->name('envio/solicitud');
Route::get('/editar/{id}','SolicitudController@change')->name('envio/solicitud');
//ruta cargar Tabla solicitudes
Route::get('/tabla-solicitudes','SolicitudController@tablasolicitudes')->name('tabla-solicitudes');
// Route::get('/tabla-mandante','SolicitudController@tablasolicitudes')->name('tabla-mandante');
//crear usuario function
Route::get('/usuarios/crauser','PruebaController@createuser')->name('cuser');


// Rutas de carga
Route::get('/upload', 'UploadImagesController@create');
Route::post('/cargarDoc1', 'UploadImagesController@store');
Route::post('/cargarDoc2', 'UploadImagesController@store1');
Route::post('/cargarDoc3', 'UploadImagesController@store2');
Route::post('/cargarDoc4', 'UploadImagesController@store3');
Route::post('/cargarDoc5', 'UploadImagesController@store4');
Route::post('/cargraCertificado/{id}', 'UploadImagesController@store5');
Route::get('/images-show', 'UploadImagesController@index');
Route::get('/descarga/{id}','UploadImagesController@download');
Route::get('/checkdirectorio','DocumentoController@checkDirectory');
Route::get('/checkdirectoriochild','DocumentoController@checkChildDirectory');

//mails
Route::post('/mails/usuarios','MailController@store');
Route::get('/mails/index','MailController@index');

//
Route::get('/contrato/{id}','ContratoController@edit');
Route::post('/contrato/update/','ContratoController@update');


// Rutas Mandante
Route::get('/solicitud-no-enviada','SolicitudController@noEnviadas');
Route::get('/solicitud-pendiente','SolicitudController@pendientes');
Route::get('/solicitud-finalizada','SolicitudController@finalizadas');
Route::get('/solicitudes','SolicitudController@index');
route::get('/solicitud/revision/{id}', 'SolicitudController@edit');

// Rutas validador
Route::get('/finalizar-solicitud/{id}','ValidadorController@validar');
