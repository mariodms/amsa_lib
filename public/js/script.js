function uploadFile(){
  if($("#fileupload").val() != "")  {
    var file_data = $('#fileupload').prop('files')[0];
    var form_data = new FormData();

    form_data.append('file', file_data);

    $.ajax({
      url: '/upload', // point to server-side PHP script
      dataType: 'text', // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: {form_data, _token: CSRF},
      type: 'post',
      success: function(data){
        // get server responce here
        alert(data);
        // clear file field
        $("#fileupload").val("");
      }
    });
  }
  else
  {
    alert("Please select file!");
  }
}
