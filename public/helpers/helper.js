$.calculaDigitoVerificador = function (rut) {
    
    // type check
    if (!rut || !rut.length || typeof rut !== 'string') {
        return -1;
    }
    // serie numerica
    var secuencia = [2,3,4,5,6,7,2,3];
    var sum = 0;
    //
    for (var i=rut.length - 1; i >=0; i--) {
        var d = rut.charAt(i)
        sum += new Number(d)*secuencia[rut.length - (i + 1)];
    };
    // sum mod 11
    var rest = 11 - (sum % 11);
    // si es 11, retorna 0, sino si es 10 retorna K,
    // en caso contrario retorna el numero
    return rest === 11 ? 0 : rest === 10 ? "K" : rest;
};

$.formatoFecha = function(fecha_) {
    var fecha = new Date(fecha_);

    //dia
    var dia = "";
    if (fecha.getDate() < 10) {
        dia = "0"+fecha.getDate();
    } else {
        dia = fecha.getDate();
    }
    //fin dia

    //mes
    var mes = "";
    if ((fecha.getMonth()+1) < 10) {
        mes = "0"+ (fecha.getMonth() + 1 );
    } else {
        mes = (fecha.getMonth() + 1);
    }
    //fin mes

    //horas
    var horas = "";
    if (fecha.getHours() < 10) {
        horas = "0"+fecha.getHours();
    } else {
        horas = fecha.getHours();
    }
    //fin horas

    //minutos
    var minutos = "";
    if (fecha.getMinutes() < 10) {
        minutos = "0"+fecha.getMinutes();
    } else {
        minutos = fecha.getMinutes();
    }
    //fin minutos

    //segundos
    var segundos = "";
    if (fecha.getSeconds() < 10) {
        segundos = "0"+fecha.getSeconds();
    } else {
        segundos = fecha.getSeconds();
    }
    //fin segundos

    return (dia +"-"+ mes +"-"+fecha.getFullYear()+" "+ horas +":"+ minutos +":"+ segundos);
};
 
$.formatoRut = function(value){
    var retorno = '';
    value = value.toString().split('').reverse().join('');
    var i = value.length;
    while(i>0) retorno += ((i%3===0&&i!=value.length)?'.':'')+value.substring(i--,i);
    return retorno;
};

$.validar_rut_ficha = function(campo){
    if ( campo.length == 0 ){ return false; }
    if ( campo.length < 8 ){ return false; }

    campo = campo.replace('-','')
    campo = campo.replace(/\s/g,"")

    var suma = 0;
    var caracteres = "1234567890kK";
    var contador = 0;    
    for (var i=0; i < campo.length; i++){
        u = campo.substring(i, i + 1);
        if (caracteres.indexOf(u) != -1)
        contador ++;
    }
    if ( contador==0 ) { return false }
    
    var rut = campo.substring(0,campo.length-1)
    var drut = campo.substring( campo.length-1 )
    var dvr = '0';
    var mul = 2;
    
    for (i= rut.length -1 ; i >= 0; i--) {
        suma = suma + rut.charAt(i) * mul
                if (mul == 7)   mul = 2
                else    mul++
    }
    res = suma % 11
    if (res==1)     dvr = 'k'
                else if (res==0) dvr = '0'
    else {
        dvi = 11-res
        dvr = dvi + ""
    }
    if ( dvr != drut.toLowerCase() ) { return false; }
    else { return true; }
};